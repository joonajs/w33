import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import birds from './bird.json';
import { Tab } from '@mui/material';


const TableExample =()=>{
    console.log(JSON.stringify(birds));
      

      
    return(
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead style={{backgroundColor: '#D3D3D3', fontWeight: 'bold'}}>
                    <TableRow>
                        <TableCell align='left' style={{fontWeight: 'bold'}}>Finnish</TableCell>
                        <TableCell align='left' style={{fontWeight: 'bold'}}>Swedish</TableCell>
                        <TableCell align='left' style={{fontWeight: 'bold'}}>English</TableCell>
                        <TableCell align='left' style={{fontWeight: 'bold'}}>Short</TableCell>
                        <TableCell align='left' style={{fontWeight: 'bold'}}>Latin</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>

                    {birds.sort((a, b) => a.finnish.localeCompare(b.finnish)).map((birds) => (
                        <TableRow
                            key={birds.name}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell align='left'>{birds.finnish}</TableCell>
                            <TableCell align='left'>{birds.swedish}</TableCell>
                            <TableCell align='left'>{birds.english}</TableCell>
                            <TableCell align='left'>{birds.short}</TableCell>
                            <TableCell align='left'>{birds.latin}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
        </div>
    )
}

export default TableExample;