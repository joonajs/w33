import Bird from './Bird';


function App() {
  return (
    <div className="App">
      <Bird />
    </div>
  );
}

export default App;
